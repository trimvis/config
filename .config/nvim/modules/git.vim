Plug 'jreybert/vimagit'

" open vimagit using <leader>g
let g:magit_show_magit_mapping = '<leader>g'
call mapping#def('g', 'Git')
